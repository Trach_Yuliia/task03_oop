package com.trach.yuliia;

import com.trach.yuliia.View.MyView;

/**
 * The Application program implements an application that
 * displays decoration in department.
 *
 * @author  Yuliia Trach
 * @version 1.0
 * @since   2019-11-09
 */
public class Application {
  public static void main(final String[] args) {
    new MyView().show();
  }
}
