package com.trach.yuliia.View;

@FunctionalInterface
public interface Printable {

  void print();
}
