package com.trach.yuliia.View;

import com.trach.yuliia.Controller.DecorationController;
import com.trach.yuliia.Model.Decoration;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Scanner;

public class MyView {

  private DecorationController controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);

  public MyView() {
    controller = new DecorationController();
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - print DecorationList");
    menu.put("2", "  2 - print DecorationTypeList");
    menu.put("3", "  3 - search decoration by type");
    menu.put("Q", "  Q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);
    methodsMenu.put("3", this::pressButton3);
  }

  private void pressButton1() {
    System.out.println(controller.getDecorationList());
  }

  private void pressButton2() {
    //System.out.println("Please input list amount:");
    //int amount = input.nextInt();
    System.out.println(controller.getDecorationTypeList());
  }

  private void pressButton3() {
    System.out.println(controller.getDecorationTypeList());
    System.out.print("Please input index of type:");
    int amount = Integer.parseInt(input.nextLine());
    LinkedList<Decoration> dec = controller.generateDecorationListByType(amount);
    for (Decoration index: dec) {
      System.out.println(index);
    }
  }


  //-------------------------------------------------------------------------

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.println("Please, select menu point.");
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
        System.out.println("Something wrong!");
      }
    } while (!keyMenu.equals("Q"));
  }
}
