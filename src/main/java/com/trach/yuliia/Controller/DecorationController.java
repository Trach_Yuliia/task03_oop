package com.trach.yuliia.Controller;

import com.trach.yuliia.Model.BusinessLogicDecoration;
import com.trach.yuliia.Model.Decoration;
import com.trach.yuliia.Model.DecorationModel;
import java.util.LinkedList;
import java.util.Map;

public class DecorationController {
  private DecorationModel decorationModel;

  public DecorationController() {
    decorationModel = new BusinessLogicDecoration();
  }

  public final LinkedList<Decoration> getDecorationList(){
    return decorationModel.getDecorationList();
  }

  public final LinkedList<Decoration> generateDecorationListByType(final int key){
    return decorationModel.generateDecorationListByType(key);
  }
  public final Map<Integer, String> getDecorationTypeList(){
    return decorationModel.getDecorationTypeList();
  }
}
