package com.trach.yuliia.Model;

import java.util.LinkedList;
import java.util.Map;

public class BusinessLogicDecoration implements DecorationModel {

  private DecorationDomain decorationDomain;

  public BusinessLogicDecoration() {
    decorationDomain = new DecorationDomain();
  }

  @Override
  public LinkedList<Decoration> getDecorationList() {
    return decorationDomain.getDecorationList();
  }

  @Override
  public LinkedList<Decoration> generateDecorationListByType(int keyType) {
    return decorationDomain.generateDecorationListByType(keyType);
  }

  @Override
  public Map<Integer, String> getDecorationTypeList() {
    return decorationDomain.getDecorationTypeList();
  }

}
