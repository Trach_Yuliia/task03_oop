package com.trach.yuliia.Model;

import java.awt.Color;

public class GardenDecoration extends Decoration {

  private int waterProofLevel;

  public GardenDecoration(String decorationName, DecorationType decorationType,
      double decorationPrice, Color decorationColors, String decorationMaterial,
      int waterProofLevel) {
    super(decorationName, decorationType, decorationPrice, decorationColors, decorationMaterial);
    this.waterProofLevel = waterProofLevel;
  }

  public int getWaterProofLevel() {
    return waterProofLevel;
  }

  public void setWaterProofLevel(int waterProofLevel) {
    this.waterProofLevel = waterProofLevel;
  }

  @Override
  public String toString() {
    return "GardenDecoration{" + super.toString() +
        "waterProofLevel=" + waterProofLevel +
        '}';
  }
}
