package com.trach.yuliia.Model;

import java.awt.Color;

public class ChristmasTreeDecoration extends Decoration {

  public ChristmasTreeDecoration(String decorationName,
      DecorationType decorationType, double decorationPrice,
      Color decorationColors, String decorationMaterial) {
    super(decorationName, decorationType, decorationPrice, decorationColors, decorationMaterial);
  }

  @Override
  public String toString() {
    return "ChristmasTreeDecoration:" + super.toString();
  }
}
