package com.trach.yuliia.Model;

public enum DecorationType {
  TOY,
  GARLAND,
  LIGHTING,
  CANDLES,
  STATES,
  STAFFED_TOY,
  DECORATIONS_ON_THE_TABLE
}
