package com.trach.yuliia.Model;

import java.util.LinkedList;
import java.util.Map;

public interface DecorationModel {

  LinkedList<Decoration> getDecorationList();

  LinkedList<Decoration> generateDecorationListByType(int keyType);

  Map<Integer, String> getDecorationTypeList();


}
