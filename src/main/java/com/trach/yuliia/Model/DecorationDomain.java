package com.trach.yuliia.Model;

import java.awt.Color;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;


public class DecorationDomain {

  private LinkedList<Decoration> decorationList;
  private LinkedList<Decoration> decorationListByType;
  private Map<Integer, String> decorationTypeList;
  private int keyType;

  private LinkedList<Decoration> christmasTreeDecorationList;
  private LinkedList<Decoration> gardenDecorationList;
  private LinkedList<Decoration> homeDecorationList;
  private LinkedList<Decoration> houseDecorationList;

  public DecorationDomain() {
    decorationList = new LinkedList<>();
    generateDecorationList();
    decorationTypeList = new LinkedHashMap<>();
    generateDecorationTypeList();
    decorationListByType = new LinkedList<>();
  }


  private void generateDecorationList() {
    decorationList.add(gardenDecorationState);
    decorationList.add(homeDecorationTable);
    decorationList.add(houseDecorationCandle);
    decorationList.add(christmasTreeGarland);
    decorationList.add(christmasTreeStuffedToy);
    decorationList.add(christmasTreeToy);
    decorationList.add(gardenDecorationLight);
    decorationList.add(homeDecorationTableCloth);
    decorationList.add(houseDecorationGarland);
  }

  public void generateDecorationTypeList() {
    decorationTypeList.put(1, "Christmas Tree Decorations");
    decorationTypeList.put(2, "Garden decorations");
    decorationTypeList.put(3, "Home decorations");
    decorationTypeList.put(4, "House decorations");

  }

  Decoration christmasTreeToy = new ChristmasTreeDecoration("Heart", DecorationType.TOY,
      9.90, Color.lightGray, "wood");
  Decoration christmasTreeGarland = new ChristmasTreeDecoration("CottonBalls",
      DecorationType.GARLAND,
      179.0, Color.CYAN, "LED's");
  Decoration christmasTreeStuffedToy = new ChristmasTreeDecoration("Gnome",
      DecorationType.STAFFED_TOY,
      30.60, Color.green, "Fabric");

  Decoration gardenDecorationState = new GardenDecoration("Deer", DecorationType.STATES,
      845.0, Color.PINK, "Ceramics", 10);
  Decoration gardenDecorationLight = new GardenDecoration("Lighter", DecorationType.LIGHTING,
      1000, Color.YELLOW, "LED's", 7);

  Decoration homeDecorationTable = new HomeDecoration("Napkin",
      DecorationType.DECORATIONS_ON_THE_TABLE,
      18.14, Color.RED, "Cellulose");
  Decoration homeDecorationTableCloth = new HomeDecoration("Table Cloth",
      DecorationType.DECORATIONS_ON_THE_TABLE,
      235.99, Color.orange, "Cotton");

  Decoration houseDecorationCandle = new HouseDecoration("Candle", DecorationType.CANDLES,
      200, Color.YELLOW, "Parafin", 2);
  Decoration houseDecorationGarland = new HouseDecoration("Garland", DecorationType.GARLAND,
      654.78, Color.WHITE, "LED's", 10);


  public LinkedList<Decoration> getChristmasTreeDecoration() {
    christmasTreeDecorationList = new LinkedList<>();
    christmasTreeDecorationList.add(christmasTreeToy);
    christmasTreeDecorationList.add(christmasTreeGarland);
    christmasTreeDecorationList.add(christmasTreeStuffedToy);
    return christmasTreeDecorationList;
  }

  public LinkedList<Decoration> getGardenTreeDecoration() {
    gardenDecorationList = new LinkedList<>();
    gardenDecorationList.add(gardenDecorationState);
    gardenDecorationList.add(gardenDecorationLight);
    return gardenDecorationList;
  }

  public LinkedList<Decoration> getHomeDecoration() {
    homeDecorationList = new LinkedList<>();
    homeDecorationList.add(homeDecorationTable);
    homeDecorationList.add(homeDecorationTableCloth);
    return homeDecorationList;
  }

  public LinkedList<Decoration> getHouseDecoration() {
    houseDecorationList = new LinkedList<>();
    houseDecorationList.add(houseDecorationGarland);
    houseDecorationList.add(houseDecorationCandle);
    return houseDecorationList;
  }

  public LinkedList<Decoration> generateDecorationListByType(int keyType) {
    switch (keyType) {
      case 1:
        decorationListByType = getChristmasTreeDecoration();
        break;
      case 2:
        decorationListByType = getGardenTreeDecoration();
        break;
      case 3:
        decorationListByType = getHomeDecoration();
        break;
      case 4:
        decorationListByType = getHouseDecoration();
        break;
      default:
        System.out.println("The list is empty");

    }
    return decorationListByType;
  }

  public Map<Integer, String> getDecorationTypeList() {
    return decorationTypeList;
  }

  public void setDecorationTypeList(Map<Integer, String> decorationTypeList) {
    this.decorationTypeList = decorationTypeList;
  }

  public LinkedList<Decoration> getDecorationList() {
    return decorationList;
  }

  public void setDecorationList(LinkedList<Decoration> decorationList) {
    this.decorationList = decorationList;
  }


  public int getKeyType() {
    return keyType;
  }

  public void setKeyType(int keyType) {
    this.keyType = keyType;
  }

  public LinkedList<Decoration> getDecorationListByType() {
    return decorationListByType;
  }

  public void setDecorationListByType(
      LinkedList<Decoration> decorationListByType) {
    this.decorationListByType = decorationListByType;
  }

}
