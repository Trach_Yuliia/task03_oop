package com.trach.yuliia.Model;

import java.awt.Color;

public class HouseDecoration extends Decoration {

  private int waterProofLevel;

  public HouseDecoration(String decorationName, DecorationType decorationType,
      double decorationPrice,
      Color decorationColors, String decorationMaterial, int waterProofLevel) {
    super(decorationName, decorationType, decorationPrice, decorationColors, decorationMaterial);
    this.waterProofLevel = waterProofLevel;
  }

  @Override
  public String toString() {
    return "HouseDecoration:" + super.toString()
        + "waterProofLevel=" + waterProofLevel
        + '}';
  }
}
