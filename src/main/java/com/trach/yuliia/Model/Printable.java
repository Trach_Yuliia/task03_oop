package com.trach.yuliia.Model;

@FunctionalInterface
public interface Printable {

  void print();
}
