package com.trach.yuliia.Model;

import java.awt.Color;

public class HomeDecoration extends Decoration {

  public HomeDecoration(String decorationName, DecorationType decorationType,
      double decorationPrice,
      Color decorationColors, String decorationMaterial) {
    super(decorationName, decorationType,
        decorationPrice, decorationColors, decorationMaterial);
  }

  @Override
  public String toString() {
    return "HomeDecoration:" + super.toString();
  }
}
