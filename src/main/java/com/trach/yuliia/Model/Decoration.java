package com.trach.yuliia.Model;

import java.awt.Color;

public abstract class Decoration {

  private String decorationName;
  private DecorationType decorationType;
  private double decorationPrice;
  private Color decorationColors;
  private String decorationMaterial;

  public Decoration(String decorationName, DecorationType decorationType,
      double decorationPrice,
      Color decorationColors, String decorationMaterial) {
    this.decorationName = decorationName;
    this.decorationType = decorationType;
    this.decorationPrice = decorationPrice;
    this.decorationColors = decorationColors;
    this.decorationMaterial = decorationMaterial;
  }

  public String getDecorationName() {
    return decorationName;
  }

  public void setDecorationName(String decorationName) {
    this.decorationName = decorationName;
  }

  public DecorationType getDecorationType() {
    return decorationType;
  }

  public void setDecorationType(DecorationType decorationType) {
    this.decorationType = decorationType;
  }

  public double getDecorationPrice() {
    return decorationPrice;
  }

  public void setDecorationPrice(double decorationPrice) {
    this.decorationPrice = decorationPrice;
  }

  public Color getDecorationColors() {
    return decorationColors;
  }

  public void setDecorationColors(Color decorationColors) {
    this.decorationColors = decorationColors;
  }

  public String getDecorationMaterial() {
    return decorationMaterial;
  }

  public void setDecorationMaterial(String decorationMaterial) {
    this.decorationMaterial = decorationMaterial;
  }

  @Override
  public String toString() {
    return
        "decorationName='" + decorationName + '\'' +
        ", decorationType=" + decorationType +
        ", decorationPrice=" + decorationPrice +
        ", decorationColors=" + decorationColors +
        ", decorationMaterial='" + decorationMaterial + '\'' +
        '}'+ "\n";
  }
}

